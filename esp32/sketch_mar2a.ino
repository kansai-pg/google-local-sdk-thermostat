#include <SPI.h>
#include <Ethernet.h>
#include <SSLClient.h>
#include "GetRealtimeDatabase.h"
#include "trust_anchors.h"
#include <FastLED.h>
#include <esp_task_wdt.h>
#define NUM_LEDS 100

CRGB leds[NUM_LEDS];
RealtimeDatabase realtimeDB;
byte mac[6] = {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED};

void blinkLED(CRGB color, int duration) {
  int numBlinks = 5;  // Number of blinks
  for (int i = 0; i < numBlinks; i++) {
    fill_solid(leds, NUM_LEDS, color);
    FastLED.show();
    delay(duration);
    fill_solid(leds, NUM_LEDS, CRGB::Black);
    FastLED.show();
    delay(duration);
  }
}

void setup() {
    Serial.begin(115200);
    SPI.begin(22, 23, 33, -1);
    Ethernet.init(19);
    esp_task_wdt_init(60, true);
    esp_task_wdt_add(NULL);

    if (!sht31.begin(0x45)) {
      Serial.println("Could not find a valid SHT31 sensor, check wiring!");
      blinkLED(CRGB::Red, 10000);  // Continuous red LED blinking
      ESP.restart();
    }

    if (Ethernet.linkStatus() == LinkON) {
      Ethernet.begin(mac);
    } else {
      Serial.println("Ethernet cable not connected, or no link detected");
      blinkLED(CRGB::Red, 10000);  // Continuous red LED blinking
      ESP.restart();
    }

    ac_controller.begin();
    irsend.begin();
    FastLED.addLeds<NEOPIXEL, 27>(leds, NUM_LEDS);
    realtimeDB.getServerSentEvents(); 
}
void loop() {
  esp_task_wdt_reset();
  realtimeDB.putData();
  realtimeDB.checkServerRespons();  // Firebaseからのデータ受信
}
