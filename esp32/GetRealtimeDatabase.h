#ifndef GET_REALTIME_DATABASE_H
#define GET_REALTIME_DATABASE_H

#include <SPI.h>
#include <Ethernet.h>
#include <SSLClient.h>
#include <ArduinoJson.h>
#include "trust_anchors.h"
#include <Adafruit_SHT31.h>
#include <IRac.h>
#include <IRsend.h>


const char* host = "testpro-21356-default-rtdb.firebaseio.com";
String user_path = "/users/google-oauth2|101581906579469553343";
String user_path2 = "/Ambient/google-oauth2|101581906579469553343";

// putData用とgetServerSentEvents用で分けている
// こうしないとコネクションエラーが発生する（同時に接続できない）
EthernetClient ethernet_client;
EthernetClient ethernet_client2;
SSLClient client(ethernet_client, TAs, (size_t)TAs_NUM, 2);
SSLClient client2(ethernet_client2, TAs, (size_t)TAs_NUM, 2);
Adafruit_SHT31 sht31 = Adafruit_SHT31();
IRToshibaAC ac_controller(25);
IRsend irsend(25);

bool isFirstResponse = true;

class RealtimeDatabase {
private:
    unsigned long dataMillis = 0;
    const unsigned long interval = 60000;

public:
    void getServerSentEvents() {
        String req_url = user_path + ".json";
        Serial.println("Connecting to: " + String(host));
        
        if (!client.connect(host, 443)) {
            Serial.println("Connection failed");
            return;
        }

        client.print("GET " + req_url + " HTTP/1.1\r\n");
        client.print("Host: " + String(host) + "\r\n");
        client.print("Accept: text/event-stream\r\n");
        client.print("Connection: keep-alive\r\n\r\n");
    }

    void checkServerRespons() {
        if (!client.available()) return;

        String data = client.readStringUntil('\n');
        if (data.startsWith("data: ")) {
            data = data.substring(6);
            Serial.println("Received Data: " + data);
            
            StaticJsonDocument<256> doc;
            DeserializationError error = deserializeJson(doc, data);
            if (error) {
                Serial.println("deserializeJson() failed: " + String(error.f_str()));
                return;
            }

            JsonObject obj = doc["data"];
            if (isFirstResponse) {
                isFirstResponse = false;
                return;
            }

            if (obj.containsKey("lightOnOff")) {
                bool lightOn = obj["lightOnOff"];
                if (lightOn) {
                  irsend.sendNEC(0x41B6659A);
                  Serial.println("on");
                } else {
                  irsend.sendNEC(0x41B67D82);
                  Serial.println("off");
                }
            }

            if (obj.containsKey("temperatureSetpoint")) {
                int tempSet = obj["temperatureSetpoint"];
                Serial.println("エアコン設定温度: " + String(tempSet));
                ac_controller.on();
                ac_controller.setTemp(tempSet);
                Serial.println("thermostatTemperatureSetpoint: " + String(tempSet));
                reportState();
            }

            if (obj.containsKey("thermostatMode")) {
                const char* mode = obj["thermostatMode"];
                Serial.println("モード: " + String(mode));
                ac_controller.on();

                if (mode == "off") {
                ac_controller.off();
                Serial.println("off");

                } else if (mode == "cool") {
                  ac_controller.setMode(kToshibaAcCool);
                  Serial.println("thermostatMode: cool");

                } else if (mode == "heat") {
                  ac_controller.setMode(kToshibaAcHeat);
                  Serial.println("thermostatMode: heat");

                } else if (mode == "dry") {
                  ac_controller.setMode(kToshibaAcDry);
                  Serial.println("thermostatMode: dry");

                } else if (mode == "fan-only") {
                  ac_controller.setMode(kToshibaAcFan);
                  Serial.println("thermostatMode: fan-only");
                }
                reportState();
            }
        }
    }

    void putData() {
        unsigned long currentMillis = millis();
        if (currentMillis - dataMillis < interval) {
            return;  // 60秒経過するまで処理をスキップ
        }
        dataMillis = currentMillis;  // タイマー更新

        String req_url = user_path2 + ".json";

        // JSONデータを作成
        StaticJsonDocument<256> doc;
        // 温度センサーから読み取った値を挿入
        doc["thermostatHumidityAmbient"] = sht31.readHumidity();
        doc["thermostatTemperatureAmbient"] = sht31.readTemperature();

        String jsonData;
        serializeJson(doc, jsonData);

        // サーバーに接続
        if (!client2.connect(host, 443)) {
            Serial.println("Connection failed");
            return;
        }

        Serial.println("Sending PATCH request to: " + String(host) + req_url);

        // HTTPリクエストの作成
        client2.print("PATCH " + req_url + " HTTP/1.1\r\n");
        client2.print("Host: " + String(host) + "\r\n");
        client2.print("Content-Type: application/json\r\n");
        client2.print("Content-Length: " + String(jsonData.length()) + "\r\n");
        client2.print("Connection: close\r\n");
        client2.print("\r\n"); // ヘッダー終了の空行
        client2.print(jsonData);  // JSONデータを送信

        Serial.println("Data sent: " + jsonData);

        // レスポンスの取得
        while (client2.connected()) {
            String resp_str = client2.readStringUntil('\n');
            Serial.println(resp_str);
            if (resp_str == "\r") { // 空行でヘッダー終了
                Serial.println("------------- Response Headers Received");
                break;
            }
        }
        client2.stop();  // クライアントを停止
    }

    void reportState() {
      ac_controller.setFan(5);
      // エアコン or 照明 へ赤外線を送る
      ac_controller.send();
    }

};

#endif
